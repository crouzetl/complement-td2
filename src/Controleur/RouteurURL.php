<?php

namespace TheFeed\Controleur;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use TheFeed\Lib\Conteneur;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Exception\NoConfigurationExecption;
use Symfony\Component\Routing\Exception\MethodNotAllowedExecption;

class RouteurURL
{

    public static function traiterRequete()
    {
        $requete = Request::createFromGlobals();

        $routes = new RouteCollection();

        // Route feed
        $route = new Route("/", [
            "_controller" => "\TheFeed\Controleur\ControleurPublication::feed",
        ]);

        $route->setMethods("GET");
        $routes->add("feed", $route);

        // Route afficherFormulaireConnexion
        $route = new Route("/connexion", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::afficherFormulaireConnexion",
            // Syntaxes équivalentes
            // "_controller" => ControleurUtilisateur::class . "::afficherFormulaireConnexion",
            // "_controller" => [ControleurUtilisateur::class, "afficherFormulaireConnexion"],
        ]);

        $route->setMethods("GET");
        $routes->add("afficherFormulaireConnexion", $route);

        $route = new Route("/connexion", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::connecter",
        ]);

        $route->setMethods("POST");
        $routes->add("connecterDepuisFormulaire", $route);

        $route = new Route("/deconnexion", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::deconnecter",
        ]);

        $route->setMethods("GET");
        $routes->add("deconnecter", $route);

        $route = new Route("/feedy", [
            "_controller" => "\TheFeed\Controleur\ControleurPublication::submitFeedy",
        ]);

        $route->setMethods("POST");
        $routes->add("submitFeedy", $route);

        $route = new Route("/inscription", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::afficherFormulaireCreation",
        ]);

        $route->setMethods("GET");
        $routes->add("afficherFormulaireCreation", $route);

        $route = new Route("/inscription", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::creerDepuisFormulaire",
        ]);

        $route->setMethods("POST");
        $routes->add("creerDepuisFormulaire", $route);

        $route = new Route("/utilisateur/{idUser}", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::pagePerso",
        ]);

        $route->setMethods("GET");
        $routes->add("pagePerso", $route);

        $contexteRequete = (new RequestContext())->fromRequest($requete);
        $associateurUrl = new UrlMatcher($routes, $contexteRequete);
        $assistantUrl = new UrlHelper(new RequestStack(), $contexteRequete);

        $assistantUrl->getAbsoluteUrl("assets/css/styles.css");

        $generateurUrl = new UrlGenerator($routes, $contexteRequete);
        $generateurUrl->generate("submitFeedy");

        Conteneur::ajouterService("ServiceAssistant", $assistantUrl);
        Conteneur::ajouterService("ServiceGenerateur", $generateurUrl);

        try {
            $donneesRoute = $associateurUrl->match($requete->getPathInfo());
            $requete->attributes->add($donneesRoute);
            $resolveurDeControleur = new ControllerResolver();
            $controleur = $resolveurDeControleur->getController($requete);
            $resolveurDArguments = new ArgumentResolver();
            $arguments = $resolveurDArguments->getArguments($requete, $controleur);
            
            $reponse = call_user_func_array($controleur, $arguments);

        } catch (ResourceNotFoundException $exception) {
            // Remplacez xxx par le bon code d'erreur
            $reponse = ControleurGenerique::afficherErreur($exception->getMessage(), 404);
        } catch (MethodNotAllowedExecption $exception) {
            // Remplacez xxx par le bon code d'erreur
            $reponse = ControleurGenerique::afficherErreur($exception->getMessage(), 405);
        } catch (NoConfigurationExecption $exception) {
            $reponse = ControleurGenerique::afficherErreur($exception->getMessage(), 401);
        }
    
        $reponse->send();
    }
}
