<?php

use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\Conteneur;

$assistantUrl = Conteneur::recupererService("ServiceAssistant");
$generateurUrl = Conteneur::recupererService("ServiceGenerateur");


/**
 * @var string $pagetitle
 * @var string $cheminVueBody
 * @var String[][] $messagesFlash
 */
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <title><?= $pagetitle ?></title>
    <meta charset="utf-8">
    <link rel="stylesheet" href=<?=  htmlspecialchars($assistantUrl->getAbsoluteUrl("assets/css/styles.css")); ?>?>
</head>

<body>
    <header>
        <div id="titre" class="center">
            <a href="<?= rawurldecode($generateurUrl->generate("feed")) ?>"><span>The Feed</span></a>
            <nav>
                <a href="<?= rawurldecode($generateurUrl->generate("feed")) ?>">Accueil</a>
                <?php
                if (!ConnexionUtilisateur::estConnecte()) {
                    ?>
                    <a href="<?= rawurldecode($generateurUrl->generate("afficherFormulaireCreation")) ?>">Inscription</a>
                    <a href="<?= rawurldecode($generateurUrl->generate("afficherFormulaireConnexion"))?>">Connexion</a>
                    <?php
                } else {
                    $idUtilisateurURL = rawurlencode(ConnexionUtilisateur::getIdUtilisateurConnecte());
                    ?>
                    <a href="<?= rawurldecode($generateurUrl->generate("pagePerso", ["idUser" => $idUtilisateurURL])) ?>">Ma
                        page</a>
                    <a href="<?= rawurldecode($generateurUrl->generate("deconnecter"))?>">Déconnexion</a>
                <?php } ?>
            </nav>
        </div>
    </header>
    <div id="flashes-container">
        <?php
        foreach (["success", "error"] as $type) {
            foreach ($messagesFlash[$type] as $messageFlash) {
        ?>
                <span class="flashes flashes-<?= $type ?>"><?= $messageFlash ?></span>
        <?php
            }
        }
        ?>
    </div>
    <?php
    require __DIR__ . "/{$cheminVueBody}";
    ?>
</body>

</html>